<?php 

interface AnimalInterface{
	public function getName();
	public function makeSound();
}

abstract class Animal implements AnimalInterface{
	public $name;
	public $sound;

	public function __construct($name,$sound)
	{
		$this->name = $name;
		$this->sound = $sound;
	}

	public function getName()
	{
		return $this->name;
	}

	public function makeSound()
	{
		return $this->sound;
	}

}

abstract class Dog extends Animal{
	public $tailLength;

	public function setTailLength($tailLength)
	{
		$this->tailLength = $tailLength;
	}

	public function getTailLength()
	{
		return $this->tailLength;
	}
}


class Poodel extends Dog{

}

class Labrador extends Dog{
	
}

class Duck extends Animal{
	public $abilityToFly;

	public function setAbilityToFly($abilityToFly)
	{
		$this->abilityToFly = $abilityToFly;
	}

	public function getAbilityToFly()
	{
		return $this->abilityToFly;
	}
}

echo "<br><strong>Пудель</strong><br>";
$pudel = new Poodel('Jack','Woof');
echo "Кличка: ",$pudel->getName(),"<br>";
echo "Издает звук: ",$pudel->makeSound(),"<br>";
$pudel->setTailLength(18);
echo "Длина хвоста: ",$pudel->getTailLength()," см<br>";

echo "<br><strong>Лабрадор</strong><br>";
$pudel = new Labrador('Russel','Bark');
echo "Кличка: ",$pudel->getName(),"<br>";
echo "Издает звук: ",$pudel->makeSound(),"<br>";
$pudel->setTailLength(24);
echo "Длина хвоста: ",$pudel->getTailLength()," см<br>";

echo "<br><strong>Утка</strong><br>";
$duck = new Duck('McDuck','Quack');
echo "Кличка: ",$duck->getName(),"<br>";
echo "Издает звук: ",$duck->makeSound(),"<br>";
$duck->setAbilityToFly(true);
echo "Способна летать: ",$duck->getAbilityToFly(),"<br>";